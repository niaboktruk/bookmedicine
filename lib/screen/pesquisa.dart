import 'dart:convert';

import 'package:bookmedicine/helpers/dialog.dart';
import 'package:bookmedicine/helpers/loading_dialog.dart';
import 'package:bookmedicine/model/cidade.dart';
import 'package:bookmedicine/model/especialidade.dart';
import 'package:bookmedicine/model/plano.dart';
import 'package:bookmedicine/model/profissional.dart';
import 'package:bookmedicine/model/uf.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../component/external_app.dart';
import '../util/global.dart' as global;

class Pesquisa extends StatefulWidget {
  final int tipo;
  Pesquisa(tipo) : this.tipo = tipo;
  @override
  _PesquisasState createState() => _PesquisasState(tipo);
}

class _PesquisasState extends State<Pesquisa> with Maps, WhatsApp, Tel, Sms {
  List<UF> _ufs = [];
  String _uf = '';
  List<Cidade> _cidades = [];
  String _cidade = '';
  List<Especialidade> _especialidades = [];
  String _especialidade = '';
  List<Plano> _planos = [];
  String _plano = '';
  List<Profissional> _pesquisa = [];

  final _nomeController = TextEditingController();
  final _estabelecimentoController = TextEditingController();

  getUFs() async {
    final response = await get(Uri.parse('${global.UrlBackend}/api/gerais/ufs'),
        headers: {"Accept": "application/json"});
    final responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      setState(() {
        _ufs = (responseJson['dados'] as List)
            .map((uf) => UF.fromJson(uf))
            .toList();
      });
    } else {
      displayDialog(context, 'Erro de API', 'As UFs não foram carregadas');
      throw Exception('Erro ao carregar UFs');
    }
  }

  getCidades(uf) async {
    buildLoadingDialog(context);
    try {
      final response = await get(
          Uri.parse('${global.UrlBackend}/api/gerais/ufs/$uf/cidades'),
          headers: {"Accept": "application/json"});
      final responseJson = jsonDecode(response.body);
      Navigator.pop(context);
      if (response.statusCode == 200)
        setState(() {
          _cidades = (responseJson['dados'] as List)
              .map((cidade) => Cidade.fromJson(cidade))
              .toList();
        });
      else {
        displayDialog(
            context, 'Erro de API', 'As cidades não foram carregadas');
        throw Exception('Erro ao carregar cidades');
      }
    } catch (_) {
      Navigator.pop(context);
      displayDialog(
          context, 'Erro na pesquisa', 'As cidades não foram carregadas');
      print("Error: " + _.toString());
      throw Exception('Erro na pesquisa');
    }
  }

  getEspecialidades() async {
    final response = await get(
        Uri.parse('${global.UrlBackend}/api/admin/config/especialidades'),
        headers: {"Accept": "application/json"});
    final responseJson = jsonDecode(response.body);

    if (response.statusCode == 200)
      setState(() {
        _especialidades = (responseJson['dados'] as List)
            .map((especialidade) => Especialidade.fromJson(especialidade))
            .toList();
      });
    else {
      displayDialog(
          context, 'Erro de API', 'As especializações não foram carregadas');
      throw Exception('Erro ao carregar especializações');
    }
  }

  getPlanos() async {
    final response = await get(
        Uri.parse('${global.UrlBackend}/api/admin/config/planosaude'),
        headers: {"Accept": "application/json"});
    final responseJson = jsonDecode(response.body);

    if (response.statusCode == 200)
      setState(() {
        _planos = (responseJson['dados'] as List)
            .map((plano) => Plano.fromJson(plano))
            .toList();
      });
    else {
      displayDialog(context, 'Erro de API', 'Os planos não foram carregados');
      throw Exception('Erro ao carregar planos');
    }
  }

  _pesquisar() async {
    String _url = '/api/adm/profissionais?';
    String _params = '';

    switch (tipo) {
      case 0:
        //_url = '';
        break;
    }
    if (_nomeController.text.trim().length > 3) {
      _params = 'nome=${_nomeController.text}&';
    } else if (_estabelecimentoController.text.trim().length > 3) {
      _params = 'nome=${_estabelecimentoController.text}&';
    }
    if (_especialidade.length > 0) {
      _params = _params + 'especialidade=$_especialidade&';
    }
    if (_uf.length > 0) {
      _params = _params + 'uf=$_uf&';
    }
    if (_cidade.length > 0) {
      _params = _params + 'cidade=$_cidade&';
    }
    if (_plano.length > 0) {
      _params = _params + 'plano_saude=$_plano&';
    }
    print(global.UrlBackend + _url + _params);
    //if (_params.length > 0) {
    try {
      buildLoadingDialog(context);
      final response = await get(
        Uri.parse(global.UrlBackend + _url + _params),
      );
      final responseJson = jsonDecode(response.body);
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (responseJson['dados'].length == 0) {
          displayDialog(
              context, 'Nenhum resultado', 'Utilize outros termos na pesquisa');
        } else {
          setState(() {
            _pesquisa = (responseJson['dados'] as List)
                .map((p) => Profissional.fromJson(p))
                .toList();
            _search = true;
          });
        }
      } else {
        displayDialog(
            context, 'Erro na pesquisa', 'Tente novamente mais tarde');
      }
    } catch (_) {
      Navigator.pop(context);
      displayDialog(context, 'Erro na pesquisa', 'Tente novamente mais tarde');
      print("Error: " + _.toString());
      throw Exception('Erro na pesquisa');
    }
    //}
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      buildLoadingDialog(context);
      setState(() {
        getUFs();
      });
      setState(() {
        getEspecialidades();
      });
      setState(() {
        getPlanos();
      });
      Navigator.pop(context);
    });
  }

  @override
  void dispose() {
    _nomeController.dispose();
    _estabelecimentoController.dispose();
    super.dispose();
  }

  Widget _cardPesquisaRede(rede, nome, especialidade, endereco) {
    return Card(
      elevation: 4,
      margin: const EdgeInsets.all(15),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.all(15),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text('Estabelecimento: $nome'),
            Text('Especialidade: $especialidade'),
            Text('Endereço: $endereco'),
            Container(
              alignment: Alignment.bottomRight,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    _medico = rede;
                  });
                },
                child: Text('+ detalhes', style: TextStyle(fontSize: 12)),
              ),
            ),
          ]),
        ),
      ]),
    );
  }

  Widget _cardPesquisaMedico(objPesquisa) {
    var espec = '';
    if (!objPesquisa.especializacoes.isEmpty)
      espec = _especialidades
          .firstWhere(
              (e) => e.id == objPesquisa.especializacoes[0].especialidadeId)
          .nome;
    return Card(
      elevation: 4,
      margin: const EdgeInsets.all(15),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.all(15),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
                '${objPesquisa.profissionalTipoId == 1 ? 'Profissional' : 'Estabelecimento'}: ${objPesquisa.nome} ${objPesquisa.sobrenome}'),
            if (espec != '') Text('Especialidade: $espec'),
            Text(
                'Endereço: ${objPesquisa.endLogradouro ?? ''}, ${objPesquisa.endNum ?? ''} ${objPesquisa.endComplemento ?? ''}'),
            Container(
              alignment: Alignment.bottomRight,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    _medico = objPesquisa.id;
                  });
                },
                child: Text('+ detalhes', style: TextStyle(fontSize: 12)),
              ),
            ),
          ]),
        ),
      ]),
    );
  }

  Widget _cardMedico(idProfissional) {
    Profissional prof = _pesquisa.firstWhere((p) => p.id == idProfissional);
    var espec = '';
    if (prof.especializacoes!.length > 0)
      espec = _especialidades
          .firstWhere((e) => e.id == prof.especializacoes![0].especialidadeId)
          .nome;
    return Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => setState(() {
                      _medico = 0;
                      _agendado = false;
                      _search = false;
                    }),
                    child: Icon(
                      Icons.arrow_back,
                      size: 30,
                    ),
                  ),
                  SizedBox(width: 60)
                ]),
          ),
          Card(
            elevation: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          '${prof.profissionalTipoId == 1 ? 'Profissional' : 'Estabelecimento'}: ${prof.nome} ${prof.sobrenome}'),
                      if (espec != '') Text('Especialidade: $espec'),
                      Text(
                          'Endereço: ${prof.endLogradouro ?? ''}, ${prof.endNum ?? ''} ${prof.endComplemento ?? ''}'),
                    ],
                  ),
                ),
              ],
            ),
          ),
          if (!_agendado)
            GestureDetector(
              onTap: () => setState(() {
                launchWhatsApp(
                    phone: prof.celComercial, message: 'Olá Book Medicine');
              }),
              child: Card(
                elevation: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/img/whatsapp_logo.png',
                                width: 25,
                              ),
                              SizedBox(width: 6),
                              Text('Pré-Agendamento:'),
                            ],
                          ),
                          Text(
                            'WhatsApp',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          if (!_agendado)
            if (prof.celComercial!.length > 0)
              GestureDetector(
                onTap: () => setState(() {
                  launchTel(phone: prof.celComercial);
                }),
                child: Card(
                  elevation: 1,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(15),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.phone,
                                      color: Colors.red,
                                    ),
                                    SizedBox(width: 6),
                                    Text('Ligar no consultório:'),
                                  ],
                                ),
                                Text(
                                  prof.celComercial ?? '',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ]),
                        ),
                      ]),
                ),
              ),
          if (!_agendado)
            GestureDetector(
              onTap: () => setState(() {
                _agendado = true;
              }),
              child: Card(
                elevation: 1,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.phonelink_ring,
                                      color: Colors.red,
                                    ),
                                    SizedBox(width: 6),
                                    Text('Pré-Agendamento:'),
                                  ]),
                              Text(
                                'Me Liga',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ]),
                      ),
                    ]),
              ),
            ),
          if (!_agendado)
            GestureDetector(
              onTap: () => setState(() {
                launchSms(phone: prof.celComercial);
              }),
              child: Card(
                elevation: 1,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.message,
                                      color: Colors.orange,
                                    ),
                                    SizedBox(width: 6),
                                    Text('Pré-Agendamento:'),
                                  ]),
                              Text(
                                'SMS',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ]),
                      ),
                    ]),
              ),
            ),
          if (!_agendado)
            if (prof.enderecoLatitude != null)
              GestureDetector(
                onTap: () => setState(() {
                  launchMap(
                      lat: prof.enderecoLatitude, long: prof.enderecoLongitude);
                }),
                child: Card(
                  elevation: 1,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(15),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/img/map.png',
                                        width: 45,
                                      ),
                                      Text('Me ajude a chegar'),
                                    ]),
                              ]),
                        ),
                      ]),
                ),
              ),
          if (_agendado)
            Card(
              elevation: 4,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Confirmação de agendamento',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 10),
                            Text(
                                'Sua solicitação foi enviada para o ${tipo == 1 ? 'estabelecimento' : 'profissional'}.'),
                            SizedBox(height: 10),
                            Text(
                                'O mesmo entrará em contato com você em breve.'),
                          ]),
                    ),
                  ]),
            ),
        ]);
  }

  Widget cidadesDropdown() {
    return DropdownButton(
      hint: new Text('Cidade'),
      value: _cidade.isNotEmpty ? _cidade : null,
      items: _cidades.map((Cidade cid) {
        return DropdownMenuItem(
          value: cid.id.toString(),
          child: new Text(cid.nome),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() => _cidade = newValue.toString());
      },
    );
  }

  _PesquisasState(this.tipo);
  final int tipo;
  bool _agendado = false;
  bool _search = false;
  int _medico = 0;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (_medico == 0)
            Card(
              elevation: 4,
              margin: const EdgeInsets.all(15),
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (this.tipo == 0)
                      Text(
                        'Profissionais',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    if (this.tipo == 1)
                      Text(
                        'Rede Credenciada',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    if (this.tipo == 2)
                      Text(
                        'Planos de Saúde',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    if (this.tipo == 3)
                      Text(
                        'Especialidades',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    if (this.tipo == 0)
                      TextField(
                        controller: _nomeController,
                        decoration: InputDecoration(
                          labelText: 'Nome do Profissional',
                        ),
                      ),
                    if (this.tipo == 1)
                      TextField(
                        controller: _estabelecimentoController,
                        decoration: InputDecoration(
                          labelText: 'Nome do Estabelecimento',
                        ),
                      ),
                    if (this.tipo == 2)
                      DropdownButton(
                        hint: Text('Planos de Saúde'),
                        isExpanded: true,
                        value: _plano.isNotEmpty ? _plano : null,
                        onChanged: (newValue) {
                          setState(() {
                            _plano = newValue.toString();
                          });
                        },
                        items: _planos
                            .map((Plano pl) => DropdownMenuItem(
                                value: pl.id.toString(), child: Text(pl.nome)))
                            .toList(),
                      ),
                    DropdownButton(
                      hint: Text('Especialização principal'),
                      isExpanded: true,
                      value: _especialidade.isNotEmpty ? _especialidade : null,
                      onChanged: (newValue) {
                        setState(() {
                          _especialidade = newValue.toString();
                        });
                      },
                      items: _especialidades
                          .map((Especialidade esp) => DropdownMenuItem(
                              value: esp.id.toString(), child: Text(esp.nome)))
                          .toList(),
                    ),
                    DropdownButton(
                      isExpanded: true,
                      hint: Text('UF'),
                      value: _uf.isNotEmpty ? _uf : null,
                      onChanged: (newValue) {
                        setState(() {
                          _cidade = "";
                          _cidades = [];
                          _uf = newValue.toString();
                        });
                        getCidades(newValue);
                      },
                      items: _ufs
                          .map((UF uf) => DropdownMenuItem(
                              value: uf.id.toString(), child: Text(uf.cod)))
                          .toList(),
                    ),
                    if (_uf.isNotEmpty) cidadesDropdown(),
                    if (this.tipo != 2)
                      DropdownButton(
                        hint: Text('Planos de Saúde'),
                        isExpanded: true,
                        value: _plano.isNotEmpty ? _plano : null,
                        onChanged: (newValue) {
                          setState(() {
                            _plano = newValue.toString();
                          });
                        },
                        items: _planos
                            .map((Plano pl) => DropdownMenuItem(
                                value: pl.id.toString(), child: Text(pl.nome)))
                            .toList(),
                      ),
                    Center(
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _pesquisar();
                          });
                        },
                        child: Text('Pesquisar'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          if (_search && _medico == 0)
            ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: _pesquisa.length, // the length
                itemBuilder: (context, index) {
                  return _cardPesquisaMedico(_pesquisa[index]);
                }),
          if (_medico > 0) _cardMedico(_medico),
        ],
      ),
    );
  }
}
