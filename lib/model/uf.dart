class UF {
  final int id;
  final String cod;
  final String name;
  final int statusId;

  UF(
      {required this.id,
      required this.cod,
      required this.name,
      required this.statusId});

  factory UF.fromJson(Map<String, dynamic> json) {
    return UF(
      id: json['id'],
      cod: json['cod'],
      name: json['name'],
      statusId: json['status_id'],
    );
  }
}
