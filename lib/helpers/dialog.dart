import 'package:flutter/material.dart';

void displayDialog(BuildContext context, String title, String text) =>
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          title: title != ""
              ? Text(title,
                  style: TextStyle(
                      fontSize: 22,
                      color: Theme.of(context).accentColor,
                      fontWeight: FontWeight.bold))
              : null,
          content: Text(text)),
    );
