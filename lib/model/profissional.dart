class Profissional {
  final int? id;
  final int? profissionalTipoId;
  final String? nome;
  final String? sobrenome;
  final String? doc;
  final String? foto;
  final String? emailPessoal;
  final String? emailComercial;
  final String? telPessoal;
  final String? celPessoal;
  final String? telComercial;
  final String? celComercial;
  final int? celComercialWhatsapp;
  final int? celComercialMeLiga;
  final int? celComercialSms;
  final String? endLogradouro;
  final String? endNum;
  final String? endComplemento;
  final String? endBairro;
  final int? cidadeId;
  final int? ufId;
  final String? endCep;
  final num? enderecoLatitude;
  final num? enderecoLongitude;
  final String? planoComercialId;
  final int? userIdCaptador;
  final int? statusId;
  final String? createdAt;
  final String? updatedAt;
  final String? fotoImg;
  final List<CRM>? crms;
  final List<Especializacoes>? especializacoes;
  final List<PlanosSaude>? planossaude;

  Profissional(
      {this.id,
      this.profissionalTipoId,
      this.nome,
      this.sobrenome,
      this.doc,
      this.foto,
      this.emailPessoal,
      this.emailComercial,
      this.telPessoal,
      this.celPessoal,
      this.telComercial,
      this.celComercial,
      this.celComercialWhatsapp,
      this.celComercialMeLiga,
      this.celComercialSms,
      this.endLogradouro,
      this.endNum,
      this.endComplemento,
      this.endBairro,
      this.cidadeId,
      this.ufId,
      this.endCep,
      this.enderecoLatitude,
      this.enderecoLongitude,
      this.planoComercialId,
      this.userIdCaptador,
      this.statusId,
      this.createdAt,
      this.updatedAt,
      this.fotoImg,
      this.crms,
      this.especializacoes,
      this.planossaude});

  factory Profissional.fromJson(Map<String, dynamic> json) {
    List<CRM> _crmLista =
        (json['crms'] as List).map((i) => CRM.fromJson(i)).toList();
    List<Especializacoes> _especializacoesLista =
        (json['especializacoes'] as List)
            .map((i) => Especializacoes.fromJson(i))
            .toList();
    List<PlanosSaude> _planosSaudeLista = (json['planossaude'] as List)
        .map((i) => PlanosSaude.fromJson(i))
        .toList();
    return Profissional(
        id: json['id'],
        profissionalTipoId: json['profissional_tipo_id'],
        nome: json['nome'],
        sobrenome: json['sobrenome'],
        doc: json['doc'],
        foto: json['foto'],
        emailPessoal: json['email_pessoal'],
        emailComercial: json['email_comercial'],
        telPessoal: json['tel_pessoal'],
        celPessoal: json['cel_pessoal'],
        telComercial: json['tel_comercial'],
        celComercial: json['cel_comercial'],
        celComercialWhatsapp: json['cel_comercial_whatsapp'],
        celComercialMeLiga: json['cel_comercial_me_liga'],
        celComercialSms: json['cel_comercial_sms'],
        endLogradouro: json['end_logradouro'],
        endNum: json['end_num'],
        endComplemento: json['end_complemento'],
        endBairro: json['end_bairro'],
        cidadeId: json['cidade_id'],
        ufId: json['uf_id'],
        endCep: json['end_cep'],
        enderecoLatitude: json['endereco_latitude'],
        enderecoLongitude: json['endereco_longitude'],
        planoComercialId: json['plano_comercial_id'],
        userIdCaptador: json['user_id_captador'],
        statusId: json['status_id'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        fotoImg: json['foto_img'],
        crms: _crmLista,
        especializacoes: _especializacoesLista,
        planossaude: _planosSaudeLista);
  }
}

class CRM {
  final int? id;
  final int? profissionalId;
  final String? numero;
  final int? ufId;
  final int? statusId;
  final String? createdAt;
  final String? updatedAt;

  CRM(
      {this.id,
      this.profissionalId,
      this.numero,
      this.ufId,
      this.statusId,
      this.createdAt,
      this.updatedAt});

  factory CRM.fromJson(Map<String, dynamic> json) {
    return CRM(
      id: json['id'],
      profissionalId: json['profissional_id'],
      numero: json['numero'],
      ufId: json['uf_id'],
      statusId: json['status_id'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}

class Especializacoes {
  final int? id;
  final int? profissionalId;
  final int? especialidadeId;
  final String? numero;
  final String? createdAt;
  final String? updatedAt;

  Especializacoes(
      {this.id,
      this.profissionalId,
      this.especialidadeId,
      this.numero,
      this.createdAt,
      this.updatedAt});

  factory Especializacoes.fromJson(Map<String, dynamic> json) {
    return Especializacoes(
      id: json['id'],
      profissionalId: json['profissional_id'],
      especialidadeId: json['especialidade_id'],
      numero: json['numero'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}

class PlanosSaude {
  final int? id;
  final int? profissionalId;
  final int? planoSaudeId;
  final String? createdAt;
  final String? updatedAt;

  PlanosSaude(
      {this.id,
      this.profissionalId,
      this.planoSaudeId,
      this.createdAt,
      this.updatedAt});

  factory PlanosSaude.fromJson(Map<String, dynamic> json) {
    return PlanosSaude(
      id: json['id'],
      profissionalId: json['profissional_id'],
      planoSaudeId: json['plano_saude_id'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}
