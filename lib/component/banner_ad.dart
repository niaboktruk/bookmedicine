import 'package:flutter/material.dart';
import 'dart:async';

class BannerAd extends StatefulWidget {
  @override
  _BannerAdState createState() => _BannerAdState();
}

class _BannerAdState extends State<BannerAd> {
  static bool imgChange = false;
  static Image image1 = Image.asset(
    'assets/img/banner1.jpeg',
    fit: BoxFit.cover,
    width: double.infinity,
  );
  static Image image2 = Image.asset(
    'assets/img/banner2.jpeg',
    fit: BoxFit.cover,
    width: double.infinity,
  );
  late Timer timer;

  @override
  void initState() {
    super.initState();

    timer = Timer.periodic(Duration(seconds: 10), (Timer t) {
      setState(() {
        imgChange = !imgChange;
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(image1.image, context);
    precacheImage(image2.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return imgChange ? image1 : image2;
  }
}
