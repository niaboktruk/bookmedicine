import 'dart:convert';

import 'package:bookmedicine/component/external_app.dart';
import 'package:bookmedicine/helpers/dialog.dart';
import 'package:bookmedicine/helpers/loading_dialog.dart';
import 'package:bookmedicine/model/cidade.dart';
import 'package:bookmedicine/model/especialidade.dart';
import 'package:bookmedicine/model/plano.dart';
import 'package:bookmedicine/model/uf.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../util/global.dart' as global;

class SignUp extends StatefulWidget {
  final Function selectPesquisa;
  SignUp(this.selectPesquisa);
  @override
  _SignUpState createState() => _SignUpState(selectPesquisa);
}

class _SignUpState extends State<SignUp> with WhatsApp, Maps, Tel, Sms {
  List<UF> _ufs = [];
  String _uf = '';
  String _ufU = '';
  List<Plano> _planos = [];
  String _plano = '';
  List<Cidade> _cidades = [];
  String _cidade = '';
  String _cidadeU = '';
  List<Especialidade> _especializacoes = [];
  String _especialidade1 = '';
  String _especialidade2 = '';
  String _especialidade3 = '';
  //String _ufcrm = '';

  final _nomeController = TextEditingController();
  final _sobrenomeController = TextEditingController();
  final _cpfController = TextEditingController();
  final _foneController = TextEditingController();
  final _emailController = TextEditingController();
  final _cepController = TextEditingController();
  final _enderecoController = TextEditingController();
  final _bairroController = TextEditingController();
  final _crmController = TextEditingController();
  final _rqe1Controller = TextEditingController();
  final _rqe2Controller = TextEditingController();
  final _rqe3Controller = TextEditingController();

  final _nomeUController = TextEditingController();
  final _sobrenomeUController = TextEditingController();
  final _foneUController = TextEditingController();
  final _emailUController = TextEditingController();
  final _cepUController = TextEditingController();
  final _enderecoUController = TextEditingController();
  final _numeroUController = TextEditingController();
  final _bairroUController = TextEditingController();

  var _nomeNode = FocusNode();
  var _sobrenomeNode = FocusNode();
  var _cpfNode = FocusNode();
  var _foneNode = FocusNode();
  var _emailNode = FocusNode();
  var _cepNode = FocusNode();
  var _enderecoNode = FocusNode();
  var _bairroNode = FocusNode();
  var _crmNode = FocusNode();
  var _ufNode = FocusNode();
  var _cidadeNode = FocusNode();
  var _especialidade1Node = FocusNode();
  var _rqe1Node = FocusNode();
  var _especialidade2Node = FocusNode();
  var _rqe2Node = FocusNode();
  var _especialidade3Node = FocusNode();
  var _rqe3Node = FocusNode();

  var _nomeUNode = FocusNode();
  var _sobrenomeUNode = FocusNode();
  var _foneUNode = FocusNode();
  var _emailUNode = FocusNode();
  var _cepUNode = FocusNode();
  var _enderecoUNode = FocusNode();
  var _numeroUNode = FocusNode();
  var _bairroUNode = FocusNode();
  var _ufUNode = FocusNode();
  var _planoUNode = FocusNode();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      setState(() {
        getUFs();
        getPlanos();
        getEspecializacoes();
      });
    });
  }

  @override
  void dispose() {
    _nomeController.dispose();
    _sobrenomeController.dispose();
    _cpfController.dispose();
    _foneController.dispose();
    _emailController.dispose();
    _cepController.dispose();
    _enderecoController.dispose();
    _bairroController.dispose();
    _crmController.dispose();
    _rqe1Controller.dispose();
    _rqe2Controller.dispose();
    _rqe3Controller.dispose();
    _nomeNode.dispose();
    _sobrenomeNode.dispose();
    _cpfNode.dispose();
    _foneNode.dispose();
    _emailNode.dispose();
    _cepNode.dispose();
    _enderecoNode.dispose();
    _bairroNode.dispose();
    _crmNode.dispose();
    _ufNode.dispose();
    _cidadeNode.dispose();
    _especialidade1Node.dispose();
    _rqe1Node.dispose();
    _especialidade2Node.dispose();
    _rqe2Node.dispose();
    _especialidade3Node.dispose();
    _rqe3Node.dispose();
    _nomeUController.dispose();
    _sobrenomeUController.dispose();
    _foneUController.dispose();
    _emailUController.dispose();
    _cepUController.dispose();
    _enderecoUController.dispose();
    _numeroUController.dispose();
    _bairroUController.dispose();
    _nomeUNode.dispose();
    _sobrenomeUNode.dispose();
    _foneUNode.dispose();
    _emailUNode.dispose();
    _cepUNode.dispose();
    _enderecoUNode.dispose();
    _numeroUNode.dispose();
    _bairroUNode.dispose();
    _ufUNode.dispose();
    _planoUNode.dispose();
    super.dispose();
  }

  getUFs() async {
    buildLoadingDialog(context);
    final response = await get(Uri.parse('${global.UrlBackend}/api/gerais/ufs'),
        headers: {"Accept": "application/json"});
    final responseJson = jsonDecode(response.body);
    Navigator.pop(context);
    if (response.statusCode == 200) {
      setState(() {
        _ufs = (responseJson['dados'] as List)
            .map((uf) => UF.fromJson(uf))
            .toList();
      });
    } else {
      displayDialog(context, 'Erro de API', 'As UFs não foram carregadas');
      throw Exception('Erro ao carregar UFs');
    }
  }

  getPlanos() async {
    buildLoadingDialog(context);
    final response = await get(
        Uri.parse('${global.UrlBackend}/api/admin/config/planosaude'),
        headers: {"Accept": "application/json"});
    final responseJson = jsonDecode(response.body);
    Navigator.pop(context);
    if (response.statusCode == 200) {
      setState(() {
        _planos = (responseJson['dados'] as List)
            .map((pl) => Plano.fromJson(pl))
            .toList();
      });
    } else {
      displayDialog(context, 'Erro de API', 'Os planos não foram carregados');
      throw Exception('Erro ao carregar Planos');
    }
  }

  getCidades(uf) async {
    buildLoadingDialog(context);
    final response = await get(
        Uri.parse('${global.UrlBackend}/api/gerais/ufs/$uf/cidades'),
        headers: {"Accept": "application/json"});
    final responseJson = jsonDecode(response.body);
    Navigator.pop(context);
    if (response.statusCode == 200)
      setState(() {
        _cidades = (responseJson['dados'] as List)
            .map((cidade) => Cidade.fromJson(cidade))
            .toList();
      });
    else {
      displayDialog(context, 'Erro de API', 'As cidades não foram carregadas');
      throw Exception('Erro ao carregar cidades');
    }
  }

  getEspecializacoes() async {
    final response = await get(
        Uri.parse('${global.UrlBackend}/api/admin/config/especialidades'),
        headers: {"Accept": "application/json"});
    final responseJson = jsonDecode(response.body);

    if (response.statusCode == 200)
      setState(() {
        _especializacoes = (responseJson['dados'] as List)
            .map((especialidade) => Especialidade.fromJson(especialidade))
            .toList();
      });
    else {
      displayDialog(
          context, 'Erro de API', 'As especializações não foram carregadas');
      throw Exception('Erro ao carregar especializações');
    }
  }

  salvarProfissional() async {
    if (_nomeController.text.trim().length < 3) {
      _nomeNode.requestFocus();
      displayDialog(
          context, 'Atenção', 'Preencha o nome com mais de 3 caracteres');
      return;
    }
    if (_sobrenomeController.text.trim().length < 3) {
      _sobrenomeNode.requestFocus();
      displayDialog(
          context, 'Atenção', 'Preencha o sobrenome com mais de 3 caracteres');
      return;
    }
    if (_cpfController.text.trim().length != 11) {
      _cpfNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o CPF corretamente');
      return;
    }
    if (_foneController.text.trim().length < 8) {
      _foneNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o telefone corretamente');
      return;
    }
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(_emailController.text.trim())) {
      _emailNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o e-mail corretamente');
      return;
    }
    if (_cepController.text.trim().length != 8) {
      _cepNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o CEP corretamente');
      return;
    }
    if (_enderecoController.text.trim().length < 3) {
      _enderecoNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o endereço corretamente');
      return;
    }
    if (_bairroController.text.trim().length < 3) {
      _bairroNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o bairro corretamente');
      return;
    }
    if (_uf.length == 0) {
      _ufNode.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha a UF');
      return;
    }
    if (_cidade.length == 0) {
      _cidadeNode.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha a cidade');
      return;
    }
    if (_crmController.text.trim().length < 4) {
      _crmNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o CRM corretamente');
      return;
    }
    /* if (_especialidade1.length == 0) {
      _especialidade1Node.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha a especialização principal');
      return;
    }
    if (_rqe1Controller.text.trim().length < 3) {
      _rqe1Node.requestFocus();
      displayDialog(context, 'Atenção', 'Digite a RQE');
      return;
    }
    if (_especialidade2.length == 0) {
      _especialidade2Node.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha a segunda especialização');
      return;
    }
    if (_rqe2Controller.text.trim().length < 3) {
      _rqe2Node.requestFocus();
      displayDialog(context, 'Atenção', 'Digite a RQE');
      return;
    }
    if (_especialidade3.length == 0) {
      _especialidade3Node.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha a terceira especialização');
      return;
    }
    if (_rqe3Controller.text.trim().length < 3) {
      _rqe3Node.requestFocus();
      displayDialog(context, 'Atenção', 'Digite a RQE');
      return;
    } */
    if (_card == 0) {
      displayDialog(context, 'Atenção', 'Escolha um plano para continuar');
      return;
    }
    buildLoadingDialog(context);
    try {
      final response = await post(
          Uri.parse('${global.UrlBackend}/api/profissional/add'),
          headers: <String, String>{
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
          },
          body: <String, String>{
            'profissional_tipo_id': '1',
            'nome': _nomeController.text.trim(),
            'doc': _cpfController.text,
            'sobrenome': _sobrenomeController.text.trim(),
            'telefone': _foneController.text,
            'email': _emailController.text.trim(),
            'cep': _cepController.text,
            'logradouro': _enderecoController.text.trim(),
            'numero': '0',
            'complemento': '',
            'bairro': _bairroController.text.trim(),
            'cidade_id': _cidade,
            'uf_id': _uf,
            'crm': _crmController.text,
            'especialidade_id_01': _especialidade1,
            'especialidade_num_01': _rqe1Controller.text,
            'especialidade_id_02': _especialidade2,
            'especialidade_num_02': _rqe2Controller.text,
            'especialidade_id_03': _especialidade3,
            'especialidade_num_03': _rqe3Controller.text,
            'plano_comercial_id': _card.toString()
          });

      print(response.body);
      Navigator.pop(context);

      if (response.statusCode == 200) {
        setState(() {
          _cadastrado = true;
        });
      } else {
        if (response.body.contains('O email informado'))
          displayDialog(
              context, 'Erro no cadastro', 'E-mail já cadastrado no sistema');
        else
          displayDialog(context, 'Erro no cadastro',
              'Preencha todos os campos corretamente');
      }
    } catch (_) {
      displayDialog(context, 'Erro no cadastro',
          'Sistema com problemas, tente novamente mais tarde');
    }
  }

  salvarPaciente() async {
    if (_nomeUController.text.trim().length < 3) {
      _nomeUNode.requestFocus();
      displayDialog(
          context, 'Atenção', 'Preencha o nome com mais de 3 caracteres');
      return;
    }
    if (_sobrenomeUController.text.trim().length < 3) {
      _sobrenomeUNode.requestFocus();
      displayDialog(
          context, 'Atenção', 'Preencha o sobrenome com mais de 3 caracteres');
      return;
    }
    if (_foneUController.text.trim().length < 8) {
      _foneUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o telefone corretamente');
      return;
    }
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(_emailUController.text.trim())) {
      _emailUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o e-mail corretamente');
      return;
    }
    if (_cepUController.text.trim().length != 8) {
      _cepUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o CEP corretamente');
      return;
    }
    if (_enderecoUController.text.trim().length < 3) {
      _enderecoUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o endereço corretamente');
      return;
    }
    if (_numeroUController.text.trim().length == 0) {
      _numeroUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o número corretamente');
      return;
    }
    if (_bairroUController.text.trim().length < 3) {
      _bairroUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Preencha o bairro corretamente');
      return;
    }
    if (_ufU.length == 0) {
      _ufUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha a UF');
      return;
    }
    if (_cidade.length == 0) {
      _cidadeNode.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha a cidade');
      return;
    }
    if (_plano == '') {
      _planoUNode.requestFocus();
      displayDialog(context, 'Atenção', 'Escolha o plano de saúde');
      return;
    }
    buildLoadingDialog(context);
    try {
      final response = await post(
          Uri.parse('${global.UrlBackend}/api/paciente/add'),
          headers: <String, String>{
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
          },
          body: <String, String>{
            'nome': _nomeUController.text.trim(),
            'sobrenome': _sobrenomeUController.text.trim(),
            'telefone': _foneUController.text,
            'email': _emailUController.text.trim(),
            'cep': _cepUController.text,
            'logradouro': _enderecoUController.text.trim(),
            'numero': _numeroUController.text,
            'complemento': '',
            'bairro': _bairroUController.text.trim(),
            'cidade_id': _cidade,
            'uf_id': _ufU,
            'plano_saude_id': _plano.toString()
          });

      print(response.body);
      Navigator.pop(context);

      if (response.statusCode == 200) {
        setState(() {
          _cadastrado = true;
        });
      } else {
        if (response.body.contains('The doc has already been taken'))
          displayDialog(context, 'Erro no cadastro',
              'CPF já cadastrado, verifique seu e-mail');
        else if (response.body.contains('O email informado'))
          displayDialog(
              context, 'Erro no cadastro', 'E-mail já cadastrado no sistema');
        else
          displayDialog(context, 'Erro no cadastro',
              'Preencha todos os campos corretamente');
      }
    } catch (_) {
      displayDialog(context, 'Erro no cadastro',
          'Sistema com problemas, tente novamente mais tarde');
    }
  }

  Widget _cardAgendamento(String imagem, String data, String botao) {
    return Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () => setState(() {
                      _detalhesAgendamento = false;
                      _detalhesSolicitacoes = false;
                    }),
                    child: Icon(
                      Icons.arrow_back,
                      size: 30,
                    ),
                  ),
                  SizedBox(width: 30),
                  Text(
                    'Detalhes do agendamento',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).accentColor,
                      fontSize: 18,
                    ),
                  ),
                ]),
          ),
          Container(
            child: ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
              child: Image.asset(
                'assets/img/$imagem.png',
                width: 200,
                height: 200,
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Card(
            elevation: 4,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Data: $data',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                    ]),
              ),
            ]),
          ),
          Card(
            elevation: 4,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          'Status: ${_tipoStatus ? 'Confirmado' : 'Aguardando confirmação'}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                    ]),
              ),
            ]),
          ),
          Card(
            elevation: 4,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(_nome,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                      Text('Especialidades:'),
                      Text(_especialidades),
                    ]),
              ),
            ]),
          ),
          Card(
            elevation: 4,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Endereço: Rua John Lennon, 156, Centro'),
                      Text('Telefone: $_telefone'),
                    ]),
              ),
            ]),
          ),
          GestureDetector(
            onTap: () => setState(() {
              launchTel(phone: '5561991542522');
            }),
            child: Card(
              elevation: 1,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.phone,
                                    color: Colors.red,
                                  ),
                                  SizedBox(width: 8),
                                  Text('Ligar para o paciente'),
                                ]),
                          ]),
                    ),
                  ]),
            ),
          ),
          GestureDetector(
            onTap: () => setState(() {
              launchWhatsApp(
                  phone: '5561991542522', message: 'Olá Book Medicine');
            }),
            child: Card(
              elevation: 1,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/img/whatsapp_logo.png',
                                    width: 25,
                                  ),
                                  SizedBox(width: 6),
                                  Text('Enviar mensagem por WhatsApp'),
                                ]),
                          ]),
                    ),
                  ]),
            ),
          ),
          GestureDetector(
            onTap: () => setState(() {
              launchSms(phone: '5561991542522');
            }),
            child: Card(
              elevation: 1,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.message,
                                    color: Colors.orange,
                                  ),
                                  SizedBox(width: 8),
                                  Text('Enviar SMS'),
                                ]),
                          ]),
                    ),
                  ]),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: ElevatedButton(
              onPressed: () {
                setState(() {
                  _detalhesAgendamento = false;
                  _detalhesSolicitacoes = false;
                });
              },
              child: Text(_tipoStatus
                  ? 'Desmarcar Agendamento'
                  : 'Confirmar Agendamento'),
            ),
          ),
        ]);
  }

  Widget cidadesDropdown() {
    return DropdownButton(
      hint: new Text('Cidade'),
      focusNode: _cidadeNode,
      value: _cidade.isNotEmpty ? _cidade : null,
      items: _cidades.map((Cidade cid) {
        return DropdownMenuItem(
          value: cid.id.toString(),
          child: new Text(cid.nome),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() => _cidade = newValue.toString());
      },
    );
  }

  final Function selectPesquisa;
  _SignUpState(this.selectPesquisa);

  bool _detalhesAgendamento = false;
  bool _detalhesSolicitacoes = false;
  bool _cadastrado = false;
  bool _solicitacoes = false;
  bool _agendamentos = false;
  bool _detalhes = false;
  bool _tipoStatus = false;
  String _nome = '';
  String _telefone = '';
  String _especialidades = '';
  int _segmentedControlGroupValue = 1;
  int _card = 0;
  final Map<int, Widget> myTabs = const <int, Widget>{
    0: Text("Paciente"),
    1: Text("Médico")
  };

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          if (!_cadastrado)
            CupertinoSlidingSegmentedControl(
                groupValue: _segmentedControlGroupValue,
                children: myTabs,
                onValueChanged: (i) {
                  setState(() {
                    _segmentedControlGroupValue = int.parse(i.toString());
                  });
                }),
          if (!_cadastrado &&
              !_solicitacoes &&
              !_agendamentos &&
              !_detalhes &&
              !_detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _segmentedControlGroupValue == 0)
            Card(
              elevation: 4,
              margin: const EdgeInsets.all(15),
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Cadastro do usuário',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).accentColor,
                          fontSize: 18,
                        ),
                      ),
                      TextField(
                        controller: _nomeUController,
                        maxLength: 50,
                        focusNode: _nomeUNode,
                        decoration: InputDecoration(labelText: 'Nome'),
                      ),
                      TextField(
                        controller: _sobrenomeUController,
                        maxLength: 50,
                        focusNode: _sobrenomeUNode,
                        decoration: InputDecoration(labelText: 'Sobrenome'),
                      ),
                      TextField(
                        controller: _foneUController,
                        maxLength: 12,
                        focusNode: _foneUNode,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: 'Telefone'),
                      ),
                      TextField(
                        controller: _emailUController,
                        maxLength: 50,
                        focusNode: _emailUNode,
                        decoration: InputDecoration(labelText: 'E-mail'),
                      ),
                      TextField(
                        controller: _cepUController,
                        maxLength: 8,
                        focusNode: _cepUNode,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: 'CEP'),
                      ),
                      TextField(
                        controller: _enderecoUController,
                        maxLength: 50,
                        focusNode: _enderecoUNode,
                        decoration: InputDecoration(labelText: 'Endereço'),
                      ),
                      TextField(
                        controller: _numeroUController,
                        maxLength: 8,
                        focusNode: _numeroUNode,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: 'Número'),
                      ),
                      TextField(
                        controller: _bairroUController,
                        maxLength: 20,
                        focusNode: _bairroUNode,
                        decoration: InputDecoration(labelText: 'Bairro'),
                      ),
                      DropdownButton(
                        isExpanded: true,
                        hint: Text('UF'),
                        focusNode: _ufUNode,
                        value: _ufU.isNotEmpty ? _ufU : null,
                        onChanged: (newValue) {
                          setState(() {
                            _cidadeU = "";
                            _cidades = [];
                            _ufU = newValue.toString();
                          });
                          getCidades(newValue);
                        },
                        items: _ufs
                            .map((UF uf) => DropdownMenuItem(
                                value: uf.id.toString(), child: Text(uf.cod)))
                            .toList(),
                      ),
                      if (_ufU.isNotEmpty) cidadesDropdown(),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Plano de Saúde',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).accentColor,
                          fontSize: 18,
                        ),
                      ),
                      DropdownButton(
                        isExpanded: true,
                        hint: Text('Planos de saúde'),
                        focusNode: _ufUNode,
                        value: _plano.isNotEmpty ? _plano : null,
                        onChanged: (newValue) {
                          setState(() {
                            _plano = newValue.toString();
                          });
                        },
                        items: _planos
                            .map((Plano pl) => DropdownMenuItem(
                                value: pl.id.toString(), child: Text(pl.nome)))
                            .toList(),
                      ),
                      Center(
                        child: ElevatedButton(
                          onPressed: () {
                            salvarPaciente();
                          },
                          child: Text('Cadastrar'),
                        ),
                      ),
                    ]),
              ),
            ),
          if (!_cadastrado &&
              !_solicitacoes &&
              !_agendamentos &&
              !_detalhes &&
              !_detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _segmentedControlGroupValue == 1)
            Card(
              elevation: 4,
              margin: const EdgeInsets.all(15),
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Cadastro do profissional de saúde',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).accentColor,
                          fontSize: 18,
                        ),
                      ),
                      TextField(
                        controller: _nomeController,
                        maxLength: 50,
                        focusNode: _nomeNode,
                        decoration: InputDecoration(labelText: 'Nome'),
                      ),
                      TextField(
                        controller: _sobrenomeController,
                        maxLength: 50,
                        focusNode: _sobrenomeNode,
                        decoration: InputDecoration(labelText: 'Sobrenome'),
                      ),
                      TextField(
                        controller: _cpfController,
                        maxLength: 11,
                        focusNode: _cpfNode,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: 'CPF'),
                      ),
                      TextField(
                        controller: _foneController,
                        maxLength: 12,
                        focusNode: _foneNode,
                        keyboardType: TextInputType.number,
                        decoration:
                            InputDecoration(labelText: 'Telefone Comercial'),
                      ),
                      TextField(
                        controller: _emailController,
                        maxLength: 50,
                        focusNode: _emailNode,
                        decoration:
                            InputDecoration(labelText: 'E-mail Comercial'),
                      ),
                      TextField(
                        controller: _cepController,
                        maxLength: 8,
                        focusNode: _cepNode,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: 'CEP'),
                      ),
                      TextField(
                        controller: _enderecoController,
                        maxLength: 50,
                        focusNode: _enderecoNode,
                        decoration:
                            InputDecoration(labelText: 'Endereço Comercial'),
                      ),
                      TextField(
                        controller: _bairroController,
                        maxLength: 20,
                        focusNode: _bairroNode,
                        decoration: InputDecoration(labelText: 'Bairro'),
                      ),
                      DropdownButton(
                        isExpanded: true,
                        hint: Text('UF'),
                        focusNode: _ufNode,
                        value: _uf.isNotEmpty ? _uf : null,
                        onChanged: (newValue) {
                          setState(() {
                            _cidade = "";
                            _cidades = [];
                            _uf = newValue.toString();
                          });
                          getCidades(newValue);
                        },
                        items: _ufs
                            .map((UF uf) => DropdownMenuItem(
                                value: uf.id.toString(), child: Text(uf.cod)))
                            .toList(),
                      ),
                      if (_uf.isNotEmpty) cidadesDropdown(),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Dados profissionais',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).accentColor,
                          fontSize: 18,
                        ),
                      ),
                      Row(children: [
                        Flexible(
                          child: TextField(
                            controller: _crmController,
                            maxLength: 15,
                            focusNode: _crmNode,
                            decoration: InputDecoration(labelText: 'CRM'),
                          ),
                        ),
                        SizedBox(width: 30),
                        /* Flexible(
                          child: DropdownButton(
                            hint: Text('UF'),
                            value: _ufcrm.isNotEmpty ? _ufcrm : null,
                            onChanged: (newValue) {
                              setState(() {
                                _ufcrm = newValue.toString();
                              });
                            },
                            items: _ufs
                                .map((UF uf) => DropdownMenuItem(
                                    value: uf.id.toString(),
                                    child: Text(uf.cod)))
                                .toList(),
                          ),
                        ), */
                      ]),
                      Row(children: [
                        Flexible(
                          flex: 2,
                          child: DropdownButton(
                            hint: Text('Especialização principal'),
                            focusNode: _especialidade1Node,
                            isExpanded: true,
                            value: _especialidade1.isNotEmpty
                                ? _especialidade1
                                : null,
                            onChanged: (newValue) {
                              setState(() {
                                _especialidade1 = newValue.toString();
                              });
                            },
                            items: _especializacoes
                                .map((Especialidade esp) => DropdownMenuItem(
                                    value: esp.id.toString(),
                                    child: Text(esp.nome)))
                                .toList(),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: _rqe1Controller,
                            maxLength: 10,
                            focusNode: _rqe1Node,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'RQE'),
                          ),
                        ),
                      ]),
                      Row(children: [
                        Flexible(
                          flex: 2,
                          child: DropdownButton(
                            hint: Text('Especialização 02'),
                            focusNode: _especialidade2Node,
                            isExpanded: true,
                            value: _especialidade2.isNotEmpty
                                ? _especialidade2
                                : null,
                            onChanged: (newValue) {
                              setState(() {
                                _especialidade2 = newValue.toString();
                              });
                            },
                            items: _especializacoes
                                .map((Especialidade esp) => DropdownMenuItem(
                                    value: esp.id.toString(),
                                    child: Text(esp.nome)))
                                .toList(),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: _rqe2Controller,
                            maxLength: 10,
                            focusNode: _rqe2Node,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'RQE'),
                          ),
                        ),
                      ]),
                      Row(children: [
                        Flexible(
                          flex: 2,
                          child: DropdownButton(
                            hint: Text('Especialização 03'),
                            isExpanded: true,
                            focusNode: _especialidade3Node,
                            value: _especialidade3.isNotEmpty
                                ? _especialidade3
                                : null,
                            onChanged: (newValue) {
                              setState(() {
                                _especialidade3 = newValue.toString();
                              });
                            },
                            items: _especializacoes
                                .map((Especialidade esp) => DropdownMenuItem(
                                    value: esp.id.toString(),
                                    child: Text(esp.nome)))
                                .toList(),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: _rqe3Controller,
                            maxLength: 10,
                            focusNode: _rqe3Node,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'RQE'),
                          ),
                        ),
                      ]),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Escolha o plano',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).accentColor,
                          fontSize: 18,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(1),
                        child: Row(children: [
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () => setState(() => _card = 1),
                              child: Card(
                                color:
                                    _card == 1 ? Colors.lightBlue[100] : null,
                                margin: EdgeInsets.all(5),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Center(
                                          child: Text(
                                            ' FREE ',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              backgroundColor:
                                                  Theme.of(context).accentColor,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                'R\$ 0',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Text(
                                                '/Por mês',
                                                style: TextStyle(
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ]),
                                        SizedBox(height: 4),
                                        Text(
                                            'Faça parte do principal portal médico do momento.',
                                            textAlign: TextAlign.center),
                                        SizedBox(height: 8),
                                        Text(
                                          '✓ Guia médico',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Dados de contato',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Localização',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Foto de perfil',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ]),
                                ),
                              ),
                            ),
                          ),
                          /* Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () => setState(() => _card = 2),
                              child: Card(
                                color:
                                    _card == 2 ? Colors.lightBlue[100] : null,
                                margin: EdgeInsets.all(5),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Center(
                                          child: Text(
                                            ' BASIC ',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              backgroundColor:
                                                  Theme.of(context).accentColor,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                'R\$ 49',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Text(
                                                '/Por mês',
                                                style: TextStyle(
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ]),
                                        SizedBox(height: 4),
                                        Text(
                                            'Além dos resultados, facilite o cliente a achar seu consultório.',
                                            textAlign: TextAlign.center),
                                        SizedBox(height: 8),
                                        Text(
                                          '✓ Plano Free +',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Resultado no mapa',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Como chegar',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ ⭐️ na especialidade',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ]),
                                ),
                              ),
                            ),
                          ), */
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () => setState(() => _card = 3),
                              child: Card(
                                color:
                                    _card == 3 ? Colors.lightBlue[100] : null,
                                margin: EdgeInsets.all(5),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Center(
                                          child: Text(
                                            ' STANDARD ',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              backgroundColor:
                                                  Theme.of(context).accentColor,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                'R\$ 99',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Text(
                                                '/Por mês',
                                                style: TextStyle(
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ]),
                                        SizedBox(height: 4),
                                        Text(
                                            'Que tal receber pedidos de agendamento de forma prática?',
                                            textAlign: TextAlign.center),
                                        SizedBox(height: 8),
                                        Text(
                                          '✓ Plano Basic +',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Pré agendamento',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Me liga (ou sms)',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ WhatsApp',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ]),
                                ),
                              ),
                            ),
                          ),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2),
                        child: Row(children: [
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () => setState(() => _card = 4),
                              child: Card(
                                color:
                                    _card == 4 ? Colors.lightBlue[100] : null,
                                margin: EdgeInsets.all(5),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Center(
                                          child: Text(
                                            ' PREMIUM ',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              backgroundColor:
                                                  Theme.of(context).accentColor,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                'R\$ 199',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Text(
                                                '/Por mês',
                                                style: TextStyle(
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ]),
                                        SizedBox(height: 4),
                                        Text(
                                            'O plano mais completo: todos os recursos, com Telemedicina.',
                                            textAlign: TextAlign.center),
                                        SizedBox(height: 8),
                                        Text(
                                          '✓ Plano Standard +',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Receita online',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Telemedicina',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          '✓ Page própria',
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ]),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Card(
                              margin: EdgeInsets.all(5),
                              child: null,
                            ),
                          ),
                        ]),
                      ),
                      Center(
                        child: ElevatedButton(
                          onPressed: () {
                            salvarProfissional();
                          },
                          child: Text('Cadastrar'),
                        ),
                      ),
                    ]),
              ),
            ),
          if (_cadastrado &&
              !_solicitacoes &&
              !_agendamentos &&
              !_detalhes &&
              !_detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _segmentedControlGroupValue == 0)
            Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => setState(() {
                        _cadastrado = false;
                      }),
                      child: Icon(
                        Icons.arrow_back,
                        size: 30,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Paciente: ${_nomeUController.text}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    /* Text(
                      'Plano de Saúde: $_plano',
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                      ),
                      textAlign: TextAlign.start,
                    ), */
                    SizedBox(height: 20),
                    Text(
                      'Cadastro realizado com sucesso. Um e-mail foi enviado para confirmação e senha de acesso.',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    /* SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() => selectPesquisa(0, 0));
                        },
                        child: Text('Solicitar Agendamento'),
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      'Agendamentos',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Data: 01/04/2021 14:30'),
                              Text('Profissional: Axl Rose'),
                              Text('Especialidade: Ortopedia (joelho)'),
                              Text('Endereço: Rua John Lennon, Centro'),
                              Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {
                                        setState(() => _detalhes = true);
                                      },
                                      child: Text(
                                        '+ detalhes',
                                        style: TextStyle(fontSize: 12),
                                      ),
                                    ),
                                  ]),
                            ]),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Data: 06/04/2021 14:30'),
                              Text('Profissional: David Bowie'),
                              Text('Especialidade: Ortopedia (joelho)'),
                              Text('Endereço: Rua Bob Dylan, Centro'),
                              Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {
                                        setState(() => _detalhes = true);
                                      },
                                      child: Text(
                                        '+ detalhes',
                                        style: TextStyle(fontSize: 12),
                                      ),
                                    ),
                                  ]),
                            ]),
                      ),
                    ), */
                  ]),
            ),
          if (_cadastrado &&
              !_solicitacoes &&
              !_agendamentos &&
              _detalhes &&
              !_detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _segmentedControlGroupValue == 0)
            Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () => setState(() {
                              _detalhes = false;
                            }),
                            child: Icon(
                              Icons.arrow_back,
                              size: 30,
                            ),
                          ),
                          SizedBox(width: 30),
                          Text(
                            'Detalhes do agendamento',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).accentColor,
                              fontSize: 18,
                            ),
                          ),
                        ]),
                  ),
                  Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                      child: Image.asset(
                        'assets/img/medico3.png',
                        width: 200,
                        height: 200,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Card(
                    elevation: 4,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Data: 12/05/2021 14:30h',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      )),
                                ]),
                          ),
                        ]),
                  ),
                  Card(
                    elevation: 4,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Axel Rose',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  Text('CRM 77163',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  Text('Especialidades:'),
                                  Text('✓ Ortopedia (Coluna)'),
                                  Text('✓ Ortopedia (Joelho)'),
                                ]),
                          ),
                        ]),
                  ),
                  Card(
                    elevation: 4,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      'Endereço: Rua John Lennon, 156, Centro'),
                                  Text('Telefone: 11 99173-9138'),
                                ]),
                          ),
                        ]),
                  ),
                  GestureDetector(
                    onTap: () => setState(() {
                      launchMap(lat: -23.5391141, long: -46.6635479);
                    }),
                    child: Card(
                      elevation: 1,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/img/map.png',
                                            width: 45,
                                          ),
                                          Text('Me ajude a chegar'),
                                        ]),
                                  ]),
                            ),
                          ]),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => setState(() {
                      launchTel(phone: '5561991542522');
                    }),
                    child: Card(
                      elevation: 1,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.phone,
                                            color: Colors.red,
                                          ),
                                          SizedBox(width: 8),
                                          Text('Ligar no consultório'),
                                        ]),
                                  ]),
                            ),
                          ]),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => setState(() {
                      launchWhatsApp(
                          phone: '5561991542522', message: 'Olá Book Medicine');
                    }),
                    child: Card(
                      elevation: 1,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/img/whatsapp_logo.png',
                                            width: 25,
                                          ),
                                          SizedBox(width: 6),
                                          Text('Enviar mensagem por WhatsApp'),
                                        ]),
                                  ]),
                            ),
                          ]),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => setState(() {
                      launchSms(phone: '5561991542522');
                    }),
                    child: Card(
                      elevation: 1,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.message,
                                            color: Colors.orange,
                                          ),
                                          SizedBox(width: 6),
                                          Text('Enviar SMS'),
                                        ]),
                                  ]),
                            ),
                          ]),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _detalhes = false;
                        });
                      },
                      child: Text('Desmarcar agendamento'),
                    ),
                  ),
                ]),
          if (_cadastrado &&
              !_solicitacoes &&
              !_agendamentos &&
              !_detalhes &&
              !_detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _segmentedControlGroupValue == 1)
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => setState(() {
                        _cadastrado = false;
                      }),
                      child: Icon(
                        Icons.arrow_back,
                        size: 30,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Dr(a). ${_nomeController.text}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 24,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Cadastro realizado com sucesso. Um e-mail foi enviado para confirmação e senha de acesso.',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    /* Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Exibição em Pesquisa: 814'),
                              Text('Visualização de perfil: 288'),
                              Text('Pedidos de Agendamento: 3'),
                            ]),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(children: [
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () => setState(() {
                            _solicitacoes = true;
                          }),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            color: Theme.of(context).primaryColor,
                            child: Container(
                              margin: EdgeInsets.all(15),
                              alignment: Alignment.center,
                              height: 100,
                              color: Theme.of(context).primaryColor,
                              child: Text(
                                'Nova Solicitação\n(1)',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () => setState(() {
                            _agendamentos = true;
                          }),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            color: Theme.of(context).primaryColor,
                            child: Container(
                              margin: EdgeInsets.all(15),
                              alignment: Alignment.center,
                              height: 100,
                              color: Theme.of(context).primaryColor,
                              child: Text(
                                'Agendamento\n(3)',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ]),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Avisos Book Medicine',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'A Book Medicine, além de ser seu guia médico, encurtando as distancias entre médico e paciente, possibilita o contato direto, pré-agendamento ou por SMS ou por Whatsapp, agora trás as novidades como Receita Online e Telemedicine. De um modo prático, rápido de investimento justo, a Book medicine é seu portal médico.'),
                            ]),
                      ),
                    ), */
                  ]),
            ),
          if (_cadastrado &&
              _solicitacoes &&
              !_agendamentos &&
              !_detalhes &&
              !_detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _segmentedControlGroupValue == 1)
            Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => setState(() {
                        _solicitacoes = false;
                      }),
                      child: Icon(
                        Icons.arrow_back,
                        size: 30,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      '1 nova solicitação',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(height: 10),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Paciente: John Lennon'),
                              Text('Especialidade: Ortopedia (joelho)'),
                              Text('Contato: 11 98873-7188'),
                              Text('Plano de Saúde: Unimed Paulista'),
                              Container(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      _detalhes = false;
                                      _detalhesSolicitacoes = true;
                                    });
                                  },
                                  child: Text('Confirmar',
                                      style: TextStyle(fontSize: 12)),
                                ),
                              ),
                            ]),
                      ),
                    ),
                  ]),
            ),
          if (_cadastrado &&
              !_solicitacoes &&
              !_detalhes &&
              !_detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _agendamentos &&
              _segmentedControlGroupValue == 1)
            Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => setState(() {
                        _agendamentos = false;
                      }),
                      child: Icon(
                        Icons.arrow_back,
                        size: 30,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Agendamentos atuais',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(height: 10),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Data: 01/04/2021 14:30'),
                              Text('Paciente: John Lennon'),
                              Text('Especialidade: Ortopedia (joelho)'),
                              Text('Contato: 11 98873-7188'),
                              Text('Status: Confirmado'),
                              Container(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      _detalhes = false;
                                      _detalhesAgendamento = true;
                                      _tipoStatus = true;
                                      _nome = 'John Lennon';
                                      _telefone = '11 98873-7188';
                                      _especialidades = '✓ Ortopedia (joelho)';
                                    });
                                  },
                                  child: Text('+ detalhes',
                                      style: TextStyle(fontSize: 12)),
                                ),
                              ),
                            ]),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Data: 01/04/2021 14:30'),
                              Text('Paciente: Kurt Cobain'),
                              Text('Especialidade: Ortopedia (Ombro)'),
                              Text('Contato: 11 93566-6234'),
                              Text('Status: Aguardando confirmação'),
                              Container(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      _detalhes = false;
                                      _detalhesAgendamento = true;
                                      _tipoStatus = false;
                                      _nome = 'Kurt Cobain';
                                      _telefone = '11 93566-6234';
                                      _especialidades = '✓ Ortopedia (Ombro)';
                                    });
                                  },
                                  child: Text('+ detalhes',
                                      style: TextStyle(fontSize: 12)),
                                ),
                              ),
                            ]),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Data: 01/04/2021 14:30'),
                              Text('Paciente: Bob Dylan'),
                              Text('Especialidade: Ortopedia (Pé)'),
                              Text('Contato: 11 90192-5123'),
                              Text('Status: Confirmado'),
                              Container(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      _detalhes = false;
                                      _detalhesAgendamento = true;
                                      _tipoStatus = true;
                                      _nome = 'Bob Dylan';
                                      _telefone = '11 90192-5123';
                                      _especialidades =
                                          '✓ Ortopedia (Joelho)\n✓ Ortopedia (Coluna)';
                                    });
                                  },
                                  child: Text('+ detalhes',
                                      style: TextStyle(fontSize: 12)),
                                ),
                              ),
                            ]),
                      ),
                    ),
                  ]),
            ),
          if (_cadastrado &&
              !_solicitacoes &&
              !_detalhes &&
              _detalhesAgendamento &&
              !_detalhesSolicitacoes &&
              _agendamentos &&
              _segmentedControlGroupValue == 1)
            _cardAgendamento(
                'paciente', '12/05/2021 14:30h', 'Desmarcar Agendamento'),
          if (_cadastrado &&
              !_detalhes &&
              !_detalhesAgendamento &&
              !_agendamentos &&
              _solicitacoes &&
              _detalhesSolicitacoes &&
              _segmentedControlGroupValue == 1)
            Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => setState(() {
                        _detalhesSolicitacoes = false;
                      }),
                      child: Icon(
                        Icons.arrow_back,
                        size: 30,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Confirmação de Agendamento',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(height: 10),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Paciente: John Lennon'),
                              Text('Plano de Saúde: Unimed Paulista'),
                            ]),
                      ),
                    ),
                    SizedBox(
                        width: double.infinity,
                        child: Image.asset(
                          'assets/img/calendar.png',
                        )),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('Horário: 14:30',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                            ]),
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _detalhes = false;
                            _detalhesSolicitacoes = false;
                          });
                        },
                        child: Text('Confirmar Agendamento',
                            style: TextStyle(fontSize: 12)),
                      ),
                    ),
                  ]),
            ),
        ]),
      ),
    );
  }
}
