class Cidade {
  final int id;
  final int ufId;
  final String codIbge;
  final String nome;
  final int statusId;

  Cidade(
      {required this.id,
      required this.ufId,
      required this.codIbge,
      required this.nome,
      required this.statusId});

  factory Cidade.fromJson(Map<String, dynamic> json) {
    return Cidade(
      id: json['id'],
      ufId: json['uf_id'],
      codIbge: json['cod_ibge'],
      nome: json['nome'],
      statusId: json['status_id'],
    );
  }
}
