import 'package:flutter/material.dart';

import 'home.dart';
import 'pesquisa.dart';
import 'signup.dart';
import 'about.dart';

class Tabs extends StatefulWidget {
  @override
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  static int _selectedIndex = 0;
  static int _selectDoctor = 0;

  selectPesquisa(home, doctor) {
    _selectDoctor = doctor;
    setState(() {
      _selectedIndex = home;
    });
  }

  Widget body() {
    switch (_selectedIndex) {
      case 1:
        return Pesquisa(_selectDoctor);
      case 2:
        return SignUp(selectPesquisa);
      case 3:
        return About();
      default:
        return Home(selectPesquisa);
    }
  }

  void _selecionarTab(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: 70,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(
              'assets/img/bookmedicine.png',
              fit: BoxFit.contain,
              height: 65,
            ),
          ],
        ),
      ),
      body: body(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        unselectedItemColor: Colors.grey[300],
        selectedItemColor: Colors.white,
        backgroundColor: Theme.of(context).primaryColor,
        currentIndex: _selectedIndex,
        showUnselectedLabels: true,
        onTap: _selecionarTab,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Pesquisa',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Cadastro',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.help),
            label: 'Sobre',
          ),
        ],
      ),
    );
  }
}
