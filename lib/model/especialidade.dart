class Especialidade {
  final int id;
  final String nome;
  final int statusId;

  Especialidade({required this.id, required this.nome, required this.statusId});

  factory Especialidade.fromJson(Map<String, dynamic> json) {
    return Especialidade(
      id: json['id'],
      nome: json['nome'],
      statusId: json['status_id'],
    );
  }
}
