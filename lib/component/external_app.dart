import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:url_launcher/url_launcher.dart';

mixin WhatsApp {
  Future<void> launchWhatsApp({
    @required String? phone,
    String message = '',
  }) async {
    String url;
    if (Platform.isAndroid) {
      url = 'whatsapp://send?phone=+$phone&text=${Uri.parse(message)}';
    } else {
      url = 'whatsapp://wa.me/+$phone?text=${Uri.parse(message)}';
    }

    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}

mixin Maps {
  Future<void> launchMap({
    @required num? lat,
    @required num? long,
  }) async {
    var url = 'https://www.google.com/maps/search/?api=1&query=$lat,$long';

    await canLaunch(url)
        ? await launch(
            url,
            forceSafariVC: false,
            forceWebView: false,
          )
        : throw 'Could not launch $url';
  }
}

mixin Tel {
  Future<void> launchTel({
    @required String? phone,
  }) async {
    var url = 'tel://+$phone';

    await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
  }
}

mixin Sms {
  Future<void> launchSms({
    @required String? phone,
    String message = '',
  }) async {
    String url;
    if (Platform.isAndroid) {
      url = 'sms:+$phone?body=${Uri.parse(message)}';
    } else {
      url = 'sms:+$phone&body=${Uri.parse(message)}';
    }

    await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
  }
}
