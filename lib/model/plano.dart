class Plano {
  final int id;
  final String nome;
  final int statusId;

  Plano({required this.id, required this.nome, required this.statusId});

  factory Plano.fromJson(Map<String, dynamic> json) {
    return Plano(
      id: json['id'],
      nome: json['nome'],
      statusId: json['status_id'],
    );
  }
}
