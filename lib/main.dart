import 'package:flutter/material.dart';

import 'util/routes.dart';
import 'screen/tabs.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Book Medicine',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xff60c400),
        accentColor: Color(0xff1d046c),
        fontFamily: 'RobotoCondensed',
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: Color(0xff60c400),
          ),
        ),
        iconTheme: IconThemeData(color: Color(0xff60c400)),
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                fontSize: 20,
              ),
            ),
      ),
      routes: {
        AppRoutes.HOME: (ctx) => Tabs(),
      },
    );
  }
}
