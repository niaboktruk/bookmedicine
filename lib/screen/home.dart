import 'package:flutter/material.dart';

import '../component/banner_ad.dart';

class Home extends StatelessWidget {
  final Function selectPesquisa;
  Home(this.selectPesquisa);

  final myIcons = <String, IconData>{
    'group': Icons.group,
    'business': Icons.business,
    'credit_card': Icons.credit_card,
    'medical_services': Icons.medical_services,
  };

  Widget _createCard(BuildContext context, String icon, String title) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 4,
      margin: const EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            myIcons[icon],
            size: 40,
          ),
          Text(
            title,
            textScaleFactor: 1.0,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24 - 200) / 2;
    final double itemWidth = size.width / 2;
    return Container(
      child: Column(children: [
        BannerAd(),
        Expanded(
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 2,
            padding: const EdgeInsets.all(15),
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            childAspectRatio: (itemWidth / itemHeight),
            children: [
              GestureDetector(
                child: _createCard(context, 'group', 'Profissionais'),
                onTap: () => selectPesquisa(1, 0),
              ),
              GestureDetector(
                child: _createCard(context, 'business', 'Rede Credenciada'),
                onTap: () => selectPesquisa(1, 1),
              ),
              GestureDetector(
                child: _createCard(context, 'credit_card', 'Planos de Saúde'),
                onTap: () => selectPesquisa(1, 2),
              ),
              GestureDetector(
                child:
                    _createCard(context, 'medical_services', 'Especialidades'),
                onTap: () => selectPesquisa(1, 3),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
