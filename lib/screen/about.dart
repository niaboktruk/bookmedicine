import 'package:flutter/cupertino.dart';

import '../component/external_app.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> with WhatsApp {
  int _segmentedControlGroupValue = 0;
  final Map<int, Widget> myTabs = const <int, Widget>{
    0: Text("Sobre"),
    1: Text("Termos de Uso")
  };
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            CupertinoSlidingSegmentedControl(
                groupValue: _segmentedControlGroupValue,
                children: myTabs,
                onValueChanged: (i) {
                  setState(() {
                    _segmentedControlGroupValue = int.parse(i.toString());
                  });
                }),
            if (_segmentedControlGroupValue == 0)
              Card(
                elevation: 4,
                margin: const EdgeInsets.all(15),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Sobre o Book Medicine',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'O Book Medicine é o guia médico, idealizado pelo empresário Welvis Moraes Zanotti que tem a proposta de facilitar a vida entre médico e paciente, voltado inicialmente para a cidade de Presidente Prudente e região. Um guia com serviços de busca de alta qualidade que apresenta os médicos por suas especialidades, localizações, site, planos de saúde e hospitais que atendem.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Os médicos que fazem parte do Book Medicine possuem o RQE- Registro de Qualificação de especialista conforme as normas do CREMESP SP e CFM - Conselho Federal de Medicina. O que fideliza maior credibilidade a classe médica. O médico pode o obter o RQE ao registrar seu título de especialista no CRM- Conselho Regional de Medicina Cremesp. Esta é uma resolução do CFM - Conselho Federal de Medicina, que diz ser considerado especialista, apenas o médico que possuir seu Título devidamente registrado. Este fragmento consta no artigo 4º da Resolução CFM nº 1634/2002.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'O Título de Especialista em Dermatologia, por exemplo, pode ser emitido pela AMB após aprovação no Exame do TED da SBD, ou pela Comissão Nacional de Residência Médica após a conclusão da Residência em Dermatologia. Mas é apenas quando registra o Título de Especialista no CRM que o médico recebe o número do seu RQE (Registro de Qualificação de Especialista). É importante salientar que o médico que não possui o RQE, não poderá se anunciar como especialista de determinada área. Mesmo tendo sido aprovado no exame de Título de Especialista, o CFM só considera válido o título que está registrado no CRM.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'A Propaganda Médica, outra preocupação da área médica e que está descrita na Resolução CFM nº 1.974/2011, que só pode se anunciar como especialista aquele que possui RQE; e que o número deste deve constar em quaisquer tipos de propagandas e anúncio',
                        textAlign: TextAlign.justify,
                      ),
                    ],
                  ),
                ),
              ),
            if (_segmentedControlGroupValue == 0)
              Card(
                elevation: 4,
                margin: const EdgeInsets.all(15),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Diretor Técnico',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Daniel Henrique Porto Almeida',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text('Cirurgia Geral'),
                      Text('CRM-GO 15940'),
                      Text('RQE 11624'),
                    ],
                  ),
                ),
              ),
            if (_segmentedControlGroupValue == 0)
              Card(
                elevation: 4,
                margin: const EdgeInsets.all(15),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Fale Conosco',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text('Região: Brasília-DF'),
                      Row(
                        children: [
                          Text('Fone: (18) 99777-9959'),
                          SizedBox(
                            width: 6,
                          ),
                          GestureDetector(
                            onTap: () => setState(() {
                              launchWhatsApp(
                                  phone: '5561991542522',
                                  message: 'Olá Book Medicine');
                            }),
                            child: Image.asset(
                              'assets/img/whatsapp_logo.png',
                              width: 20,
                            ),
                          ),
                        ],
                      ),
                      Text('E-mail: brasilia@bookmedicine.com.br'),
                    ],
                  ),
                ),
              ),
            if (_segmentedControlGroupValue == 1)
              Card(
                elevation: 4,
                margin: const EdgeInsets.all(15),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Termos de uso',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Os seus dados são seus. Respeitamos a sua privacidade e tratamos os seus dados como parte da nossa missão de proporcionar um serviço cada vez melhor. Essa Declaração de Privacidade descreve como obtemos, armazenamos, utilizamos e compartilhamos as suas informações.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'A proteção dos seus dados e da sua privacidade são muito importantes para o BookMedicine. Recomendamos que você conheça as nossas práticas como um todo, mas aqui estão alguns pontos importantes para que você entenda como seus dados são tratados no BookMedicine:',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '• Os dados que você cadastra em nossas plataformas são tratados de forma segura e limitamos o tratamento para suas devidas finalidades.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        '• Seus dados completos de cartão não ficam armazenados em nossas bases, mas sim em seu telefone celular ou dispositivo utilizado na compra.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        '• Você pode alterar suas preferências de comunicação no próprio aplicativo, mas ainda poderemos contatá-lo para informações sobre o seu pedido.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        '• Você poderá tirar suas dúvidas e solicitar informações a qualquer momento através da aba Ajuda em nossas plataformas.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'A quem essa Declaração se aplica?',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Esta Declaração de Privacidade, também chamada de “Declaração”, é aplicável àqueles que acessarem ou se cadastrarem em nossa Plataforma (conforme definição abaixo) e àqueles que de qualquer outra forma utilizarem os produtos do BookMedicine.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nosso aplicativo e site serão denominados, em conjunto, como Plataforma ou Plataformas. Quando houver menção aos termos “BookMedicine”, “nós” ou “nossos”, estamos nos referindo ao BookMedicine; da mesma forma, toda vez que houver menção aos termos “você”, “seu”, “sua”, estamos nos referindo a Você.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Quais dados são coletados?',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        '3.1 Dados que você nos fornece',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Quando você criar uma conta para ser um usuário registrado no BookMedicine, poderemos obter uma série de informações sobre você, tais como:',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'A) Dados do seu perfil',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Esses dados incluem seu nome, CPF (quando aplicável), e-mail, endereço de entrega, número de telefone e preferências de contato. Informamos que comunicar o CPF é opcional, sendo necessária sua indicação para certos estabelecimentos para fins fiscais.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Para criar o seu cadastro no Website ou Aplicativo do BookMedicine, você pode ainda usar a sua conta em redes sociais. Por exemplo, ao utilizar o Facebook para se inscrever no nosso Serviço, você estará permitindo que o BookMedicine acesse as informações pessoais em sua conta do Facebook, tais como seu nome e e-mail (caso os tenha cadastrado no Facebook). As informações que iremos obter, nesse caso, são as mesmas assim e dependem das suas configurações de privacidade junto ao serviço de rede social.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'B) Dados de pagamento',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Ao fazer o seu registro pelo BookMedicine, saiba que ele é gratuito. Todavia, caso algum procedimento permitido pelos Conselhos Estaduais ou Federal, é possível que, a depender do profissional escolhido, você possa fazer o pagamento diretamente ao BookMedicine ou por nossas plataformas.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Ao escolher fazer o pagamento direto no nosso Website ou Aplicativo, você poderá nos fornecer os seus dados de pagamento, tais como aqueles de cartão de crédito e de meios de pagamento parceiros.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Atenção: suas informações de pagamento online são armazenadas somente de forma anonimizada pelo BookMedicine (6 primeiros e 4 últimos dígitos do cartão), de modo que não temos acesso aos seus dados financeiros completos.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'C) Dados de localização',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Para realizarmos a correta localização do profissional buscado ou cadastrado, nós precisamos que você também nos informe a sua localização ou a localização do local em que gostaria que o profissional esteja estabelecido.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Essa localização pode ser fornecida pelo endereço que você inserir manualmente no aplicativo, ou através da localização obtida do seu dispositivo via GPS e redes móveis (torres de celular, Wi-Fi e outras modalidades de localização) e confirmada por você.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Para fins da lei n° 12.965 de 2014 (Marco Civil da Internet), ou qualquer lei que venha substituí-la, a localização fornecida será considerada como dado cadastral.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '3.2 Dados gerados durante a utilização dos serviços',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'A) Dados dos seus dispositivos',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Nós podemos coletar informações automaticamente sobre os dispositivos a partir dos quais você acessa o BookMedicine como: endereços IP, tipo de navegador e idioma, provedor de Serviços de Internet (ISP), páginas de consulta e saída, sistema operacional, informações sobre data e horário, dados sobre a sequência de cliques, fabricante do dispositivo, operadora, modelo, versão do aparelho, versão do aplicativo, versão do sistema operacional, identificador de publicidade do aparelho (IDFA), informações de acessibilidade do aparelho e redes Wi-Fi.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'B) Dados transacionais e dados sobre sua utilização',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Coletamos dados sobre suas interações em nossa plataforma, incluindo data e horário de acessos, buscas e visualizações na Plataforma. Também podemos coletar dados transacionais relacionados ao uso dos nossos serviços como detalhes do profissional, data e hora da consulta, valor cobrado, distância entre o estabelecimento e local de atendimento e método de pagamento.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'C) Dados de comunicação',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Intermediamos a comunicação entre usuários, profissional e estabelecimentos de saúde parceiros através da nossa Plataforma. Permitimos, por exemplo, que usuários, profissionais e estabelecimentos parceiros, enviem mensagens através do chat ou de telefone anonimizados. Para prestar este serviço, o BookMedicine recebe alguns dados relativos às chamadas, textos ou outras comunicações, incluindo a data e hora das comunicações e o conteúdo das comunicações.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '3.3 Informações não-pessoais',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Nós também podemos obter informações não-pessoais, ou seja, dados que não permitem a associação direta com qualquer pessoa especificamente. São exemplos de dados não-pessoais o agrupamento de pedidos por região.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Como utilizamos esses dados?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.1 Prover nossos serviços',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Podemos utilizar os dados coletados para prover, manter, melhorar e personalizar nossos produtos e serviços destinados a você, existentes ou a serem criados.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Esse tratamento inclui a utilização dos dados para: Criar e atualizar sua conta; permitir o preparo e entrega dos pedidos pelos estabelecimentos parceiros; personalizar sua conta; e prover operações internas necessárias.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Também poderemos usar seus dados pessoais para fins internos, tais como auditoria, análise de dados e pesquisas para aprimorar os produtos, serviços e comunicações com os clientes, bem como geração de análises estatísticas com respeito ao uso dos nossos serviços, incluindo tendências de consumo.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.2 Avaliação de Profissionais e Estabelecimentos da área de saúde',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Ao enviar comentários e feedback com relação à Plataforma e os serviços prestados por nossos parceiros, você nos autoriza a publicar e utilizar tais comentários e feedbacks na Plataforma, bem como a analisar, processar e tratar esse feedback de forma isolada ou agregada. Para tanto, apenas identificamos você através de seu nome de cadastro.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.3 Comunicação não comercial',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Periodicamente poderemos usar seus dados pessoais para enviar avisos e notificações importantes, como comunicados sobre compras, alterações em prazos, condições e políticas. Como estas informações são importantes para a sua interação com o BookMedicine, você não poderá optar por não receber esse tipo de comunicação, já que são inerentes ao uso do serviço.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.4 Suporte',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Também teremos acesso às suas informações quando você solicitar suporte sobre problemas com profissionais, colaboradores e parceiros, seja por meio da aba “Ajuda” no Website ou no Aplicativo.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Neste caso, poderemos utilizar informações como seus dados cadastrais para: nos certificarmos de que você é o usuário que solicitou o pedido em questão; direcionar suas dúvidas para o atendente de suporte; investigar e direcionar seu problema e; monitorar e melhorar nossos procedimentos de suporte.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.5 Gerenciamento de pagamentos efetuados online',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Segurança da informação e compliance com a legislação financeira é uma das grandes preocupações do BookMedicine. Por isso, armazenamos suas informações financeiras apenas de forma anonimizada, o que nos impede de ter acesso a seus dados financeiros completos.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Além disso, somente processamos seus dados para a finalidade especificada, qual seja, a de permitir que uma transação seja efetuada online com cobrança para você. Não se preocupe: o BookMedicine nunca processará suas informações financeiras e/ou bancárias de forma a desrespeitar as normas de compliance aplicáveis.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.6 Segurança e prevenção à fraudes',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Nós também podemos utilizar seus dados pessoais para aprimorar a nossa segurança e melhorar os serviços e as ofertas do BookMedicine destinadas a você. Dessa forma, podemos analisar e solucionar problemas técnicos, bem como identificar e coibir fraudes na utilização do nosso serviço.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.7 Marketing',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Nós poderemos usar seus dados pessoais para enviar publicidade, bem como outros materiais promocionais voltados ao marketing de nossos serviços e de novidades para você, o que inclui marketing direcionado em redes sociais e notificações push.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nesse caso, você pode optar por não receber mais notificações de marketing a qualquer momento, alterando as suas preferências pelo Aplicativo, na aba “Gerenciar notificações” em “Configurações”.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Além disso, poderemos compartilhar apenas as informações estritamente necessárias para essa finalidade com parceiros e profissionais cadastrados do BookMedicine, para fins de desenvolver campanhas de marketing mais relevantes para interessados nos produtos do BookMedicine, como sua identificação de usuário, telefone ou e-mail. O BookMedicine somente compartilhará dados com parceiros que possuírem política de privacidade que ofereça níveis compatíveis de proteção àquele oferecido por essa declaração.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.8 Pesquisas diversas',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Podemos utilizar seus dados para entrar em contato diretamente com você a fim de coletar feedbacks importantes para aprimoramento de nossos serviços. Nesta hipótese, você será questionado se deseja participar da pesquisa em questão, sempre podendo se recusar a nos ajudar em nossa melhoria diária.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '4.9 Requisições legais e regulatórias',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Seus dados poderão ser utilizados para endereçarmos reivindicações jurídicas e regulatórias relacionadas à utilização dos nossos serviços.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Como utilizamos cookies e outras tecnologias?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'O BookMedicine se utiliza de tecnologias como cookies, pixel tags, armazenamento local ou outros identificadores, tanto de dispositivos móveis ou não, ou tecnologias semelhantes (“cookies e outras tecnologias”), para uma variedade de funções.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Essas tecnologias nos ajudam a autenticar a sua conta, promover e aperfeiçoar os serviços do BookMedicine, personalizar a sua experiência e avaliar a eficácia da nossa comunicação e publicidade.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '5.1 O que são essas tecnologias?',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Cookies são pequenos arquivos armazenados no seu navegador, celular ou outro dispositivo.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Pixel tags (ou GIFs limpos, web beacons ou pixels) são pequenos blocos de código em uma página da web que permitem que elas realizem ações como ler e armazenar cookies e transmitir informações para o BookMedicine. A conexão resultante pode incluir informações como o endereço de IP de um dispositivo, a hora em que uma pessoa visualizou o pixel, um identificador associado ao navegador ou dispositivo e o tipo de navegador em uso.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Identificadores de dispositivo mobile são códigos que possibilitam a identificação do seu dispositivo mobile, seja de maneira persistente ou transitória, tais como o ID de Publicidade ou o ID de seu sistema operacional.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '5.2 Promover uma experiência personalizada',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'O BookMedicine e os seus parceiros também usam cookies e outras tecnologias para memorizar informações pessoais quando você usa o nosso Website, Aplicativo ou a rede de parceiros do BookMedicine que possam usar tecnologias similares. A nossa meta nesses casos é fazer com que a sua experiência com o BookMedicine seja mais conveniente e personalizada.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Por exemplo, saber o seu primeiro nome nos permite dar boas-vindas a você na próxima vez em que você acessar o BookMedicine. Conhecer seu país e idioma nos permite viabilizar a você uma experiência de compras personalizada e mais útil. Saber que você adquiriu determinado produto ou usou um determinado Serviço permite fazer com que sua publicidade e comunicações de e-mail sejam mais relevantes para os seus interesses.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nessa linha, podemos também usar essas informações para classificar os usuários dentro da rede BookMedicine, identificando os usuários a partir de eventos realizados, como a primeira compra ou o download do nosso aplicativo, e a partir de determinadas características de seu perfil, como usuários que pedem mais determinada categoria de comida ou que realizam mais de 4 pedidos por mês.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '5.3 Avaliar a eficácia da nossa comunicação e publicidade',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Nós usamos essas informações também para entender e analisar tendências, administrar os nossos serviços, aprender sobre o comportamento do usuário e obter informações demográficas sobre a nossa base de usuários de maneira geral. Em algumas de nossas mensagens de e-mail, nós usamos uma "URL click-through" (endereço externo) vinculada ao conteúdo do BookMedicine. Quando os clientes clicam em uma dessas URLs, os usuários são enviados para um servidor diferente antes de chegarem à página de destino no nosso serviço.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nós monitoramos esses dados de click-through para entender o interesse em determinados tópicos e avaliar a eficácia das comunicações com os nossos clientes. Se você preferir não ser monitorado dessa maneira, não clique em texto ou links contidos em mensagens de e-mail enviadas pelo BookMedicine.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Os pixel tags nos permitem enviar mensagens de e-mail em formatos que os usuários possam ler e nos dizer se o e-mail foi aberto ou não. Nós podemos usar essas informações para reduzir ou eliminar as mensagens enviadas aos usuários.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '5.4 Tecnologia de terceiros',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Além dos cookies e outras tecnologias do BookMedicine, nós podemos permitir que terceiros contratados pelo BookMedicine utilizem cookies e outras tecnologias de sua propriedade para identificar seu navegador e dispositivo, de modo a lhe oferecer publicidade direcionada do BookMedicine quando você acessa websites ou aplicativos de terceiros.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Esses terceiros podem também fornecer ao BookMedicine informações acerca do desempenho das campanhas de marketing desenvolvidas por meio do compartilhamento de dados conosco.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'A título de exemplo, algumas dessas empresas podem utilizar cookies e outras tecnologias próprios nos serviços do BookMedicine, tais como: Facebook, Google Analytics e Double Click.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Como os dados são armazenados?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        '6.1 Onde os dados são armazenados?',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Os dados que coletamos de você são armazenados em serviços de nuvem confiáveis de parceiros que podem estar localizados no Brasil ou em outros países que ofereçam Serviço de armazenamento de nuvem confiáveis e usualmente utilizados por empresas de tecnologia, tais como Estados Unidos da América (EUA) e em países da América Latina e da Europa.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Na contratação desses serviços, o BookMedicine sempre busca empresas que empregam alto nível de segurança no armazenamento de suas informações, estabelecendo contratos que não violam as definições de privacidade previstas nesta Declaração',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '6.2 Por quanto tempo os dados são armazenados?',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'O BookMedicine armazena as suas informações durante o período necessário para as finalidades apresentadas nos Termos e Condições de Uso do BookMedicine e nesta Declaração de Privacidade, respeitando o período de retenção de dados determinado pela legislação aplicável.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Caso você solicite a exclusão de sua conta, as suas informações pessoais fornecidas ao BookMedicine durante a sua utilização dos nossos serviços serão excluídos, salvo para as finalidades permitidas pela legislação de proteção de dados.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Em alguns casos, poderemos reter suas informações mesmo que você exclua sua conta, tais como nas hipóteses de guarda obrigatória de registros previstas em leis aplicáveis, se houver uma questão não resolvida relacionada à sua conta (como, por exemplo, uma reclamação ou disputa não resolvida), bem como para o exercício regular de nossos direitos, ou ainda caso seja necessário para nossos interesses legítimos, como prevenção de fraudes e aprimoramento da segurança dos nossos usuários.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'O BookMedicine transfere os dados para outros países?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'O BookMedicine poderá realizar transferências internacionais de dados para outros países, tais como Estados Unidos da América e para países da União Europeia e da América Latina, a fim de realizar algumas das atividades envolvidas nos serviços prestados a você.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Em qualquer caso de compartilhamento com parceiros ou prestadores de serviços localizados em outros países, estabelecemos contratualmente que o parceiro possua padrão de proteção de dados e segurança da informação compatível com esta Declaração e com a legislação aplicável.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Como os dados são compartilhados?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        '8.1 Provedores de serviços',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Por razões de processamento de dados em nome do BookMedicine, poderemos compartilhar dados para terceiros prestadores de serviços. Nesse caso, os dados serão tratados de forma a proteger a sua privacidade, tendo essas empresas o dever contratual de proteção compatível com a legislação aplicável e com os termos deste Declaração de Privacidade do BookMedicine.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nós podemos utilizar os serviços dessas empresas para facilitar nosso serviço, prover o serviço do BookMedicine em nosso nome e executar atividades relacionadas ao serviço, incluindo: manutenção dos serviços; gerenciamento de banco de dados, sistemas de computadores e provedores de armazenamento em nuvem; central de atendimento ao cliente; agências de publicidade.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Em relação ao pagamento de suas compras em nossa Plataforma, caso opte por pagar diretamente online, nós poderemos compartilhar seus dados de pagamento com empresas processadores de pagamento com os quais trabalhamos. Esse compartilhamento é feito com o fim exclusivo de viabilizar a compra efetivada por você.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Essas empresas ou indivíduos têm acesso à sua informação pessoal, restrito ao necessário, apenas para executar atividades solicitadas pelo BookMedicine e são obrigados a não usar ou divulgar tais informações para nenhuma outra finalidade.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '8.2 Estabelecimentos parceiros do BookMedicine',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Ao fazer um pedido em um estabelecimento parceiro por nossa Plataforma, nós podemos compartilhar algumas informações suas com esse estabelecimento parceiro, bem como fornecer dados a respeito do pedido em específico. Os dados compartilhados podem incluir nome, endereço de entrega e CPF (quando for solicitado).',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Caso você inicie um chat com o estabelecimento durante a entrega, compartilharemos os mesmos dados citados acima além do conteúdo da mensagem escrita por você.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '8.3 Parceiros comerciais',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Podemos compartilhar seus dados com empresas com atividades financeiras ou comerciais com as quais o BookMedicine mantém um relacionamento de colaboração ou parceria, a fim de oferecer promoções, produtos e serviços dessas empresas em conjunto',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nesses casos, são firmados acordos ou contratos com o objetivo de proteger a privacidade dos dados pessoais de nossos usuários e o cumprimento da legislação aplicável para que todas as medidas cabíveis para garantir o cumprimento dos padrões de confidencialidade e segurança sejam tomadas.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '8.4 Empresas do Grupo BookMedicine',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'As empresas do Grupo do BookMedicine seguem o mesmo padrão de proteção de dados pessoais descrito nesta Declaração de Privacidade do BookMedicine e obedecem às mesmas finalidades descritas neste documento. Ao compartilharmos os seus dados com essas empresas, as mesmas garantias e cuidados que o BookMedicine possui com os seus dados serão replicados pelas empresas do Grupo.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'O compartilhamento de dados com empresas do Grupo do BookMedicine terá como finalidades: (a) o desenvolvimento de novos produtos, funcionalidades e serviços, bem como sua melhoria e aperfeiçoamento; (b) a oferta de produtos e serviços que melhor atendam aos seus interesses; (c) geração de dados estatísticos e agregados acerca do uso de nossos produtos e serviços e perfis dos usuários; (d) marketing, prospecção, pesquisas de mercado, de opinião e promoção de produtos e serviços; e (e) investigações e medidas de prevenção e combate a ilícitos e fraudes.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '8.5 Em alteração de controle societário do BookMedicine',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'No caso de operações societárias, como reestruturação, fusão ou venda de ativos do BookMedicine, do grupo econômico ou de parte dele, seus dados poderão ser transferidos, desde que respeitados os termos da presente Declaração.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '8.6 Publicidade e Serviços de análise',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Para entregarmos publicidade e promoções adequadas para você, bem como aperfeiçoar nossos serviços, nós também podemos compartilhar os seus dados anonimizados com empresas especializadas em marketing e análise de dados digitais que ofereçam nível de proteção de dados compatível com esta Declaração de Privacidade.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        '8.7 Autoridades públicas',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'O BookMedicine também poderá compartilhar suas informações com autoridades policiais ou judiciais, autoridades públicas competentes ou outros terceiros, dentro e fora do país em que você reside, caso seja requerido pela legislação aplicável, por decisão judicial e por requisição de autoridades, ou se necessário para responder a processos judiciais ou para participar em eventuais litígios ou disputas de qualquer natureza.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nestas situações, o BookMedicine cooperará com as autoridades competentes na medida do estipulado por lei.',
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        'Além disso, o BookMedicine reserva a si a prerrogativa de compartilhar informações sobre seus usuários a terceiros quando houver motivos suficientes para considerar que a atividade de um usuário é suspeita, ilegal ou prejudicial ao BookMedicine ou a terceiros.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Essa prerrogativa será utilizada pelo BookMedicine quando considerar apropriado ou necessário para manter a integridade e a segurança do seu serviço, para o cumprimento dos seus Termos e Condições de Uso, para o exercício regular de seus direitos e com o intuito de cooperar com a execução e o cumprimento da lei, independentemente de existir ou não uma ordem judicial ou administrativa para tanto.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Como protegemos seus dados?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'O BookMedicine adota medidas de segurança, técnicas e administrativas aptas a proteger os dados pessoais de acessos não autorizados e de situações acidentais ou ilícitas de destruição, perda, alteração, comunicação ou qualquer forma de tratamento inadequado ou ilícito.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Utilizamos os princípios estipulados por lei, respeitando a sua privacidade e protegendo seus dados em nossos processos internos como um todo.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Só tratamos os seus dados mediante alto grau de segurança, implementando as melhores práticas em uso na indústria para a proteção de dados, tais como técnicas de criptografia, monitoramento e testes de segurança periódicos.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Como iremos notificá-lo em caso de mudanças à essa Declaração?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'O BookMedicine poderá atualizar essa Declaração de Privacidade periodicamente, sendo que a versão em vigor será sempre a mais recente. Se fizermos alguma alteração na Declaração em termos materiais, colocaremos um aviso no nosso Website, Aplicativo ou te enviar um e-mail, juntamente com a Declaração Privacidade atualizada. Para verificar a data da versão em vigor, verifique a “Data de atualização” no início desse documento.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Como exercer seus direitos enquanto titular de dados pessoais?',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'A fim de exercer seus direitos enquanto titular de dados pessoais, o BookMedicine disponibiliza meios específicos para as requisições através da aba “Ajuda” em nossas Plataformas.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Você pode clicar no menu inferior “Perfil”, depois em “Ajuda” e, em seguida, “Conta” e encontrará respostas e direcionamento específicos para sua requisição.',
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Se você tiver algum questionamento ou dúvida com relação a esta Declaração de Privacidade do BookMedicine ou qualquer prática aqui descrita, você também poderá entrar em contato conosco através da nossa página de Ajuda na Plataforma.',
                        textAlign: TextAlign.justify,
                      ),
                    ],
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
